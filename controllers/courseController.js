const Course = require("../models/Course");
const User = require("../models/User");

/*module.exports.addCourse = (reqBody) => {


	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.descriptions,
		price: reqBody.price
	})

	return User.findOne({isAdmin : true}).then(result => {
		if(result !== null){
			newCourse.save().then((newCourse, error) => {
				if (error){
					return false
				}

				return true
			})
		}

		else{
				return false
			}
		})

}*/


module.exports.addCourse = (data) =>  {
	if(data.isAdmin){
		let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.descriptions,
		price: reqBody.price
	})

		newCourse.save().then((newCourse, error) => {
				if (error){
					return false
				}

				return true
			})
	} 
	//If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve({
		message: 'User must be Admin to access this.'
	})

	return message.then((value) => {
		return value
	})
}


module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	})
}

module.exports.getActiveCourses = () => {
	return Course.find({isActive : true}).then(result => {
		return result
	})
}


module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => {
		return result
	})
}


module.exports.updateCourse = (courseId, newData) => {
	return Course.findByIdAndUpdate(courseId, {
		name: newData.name,
		description:newData.descriptions,
		price: newData.price
	})
	.then((updatedCourse, error) => {
		if(error){
			return false
		}
		return true
	})
}


module.exports.archiveCourse = (courseId) => {
	return Course.findByIdAndUpdate(courseId, {
		isActive: false
	})
	.then((archiveCourse, error) => {
		if (error){
			return false
		}

		return true
	})
}